#Serial AT command communications library

The Hayes command set (also known as the AT command set) is a specific command language originally developed by Dennis Hayes for the Hayes Smartmodem 300 baud modem in 1981.

The command set consists of a series of short text strings which can be combined to produce commands for operations such as dialing, hanging up, and changing the parameters of the connection. The vast majority of dial-up modems use the Hayes command set in numerous variations.

Modern modem modules from manufactureres such as Espressif, uBlox, Telit, SIMCOM etc. still today use this command set to communicate both control information but also data transfers from the selected endpoint.

All Challenger boards currently use some form of communications module that implements an AT command set.

This library provides an easy way to talk to the specific module on each version of the Challenger board. It knows how to reset the module and how to talk to it and it provides the caller with a simple to use API to use that functionality.
