/*
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef SERIAL_AT_H_INCLUDED
#define SERIAL_AT_H_INCLUDED

#include <Arduino.h>

typedef void (*parse_callback)(char *);

/**
 * @brief module number,Now just AT module
 *
 */
typedef enum {
    ESP_AT_MODULE_NUM = 0x01
} esp_at_module;

class SerialATClass {
    public:
        SerialATClass(int, int);
        SerialATClass();

        void init();
        void init(parse_callback _pcb);
        void parse_at_response(char *resp);
        int send_at(const char *fmt, ...);
        void handle_at();

    private:
        parse_callback _pcb;
        int _rst_pin;
        int _mode_pin;
        char at_buf[1024];
        char* at_ptrs[16];
        int rcvd_lines;
        bool _initialized;
};

#endif
