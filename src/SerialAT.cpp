/*
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <SerialAT.h>
#include <include/debug.h>

// Can be set to 0 - 3 to define different debug levels. 0 gives no debug
// info at all, 3 gives you everything.
#define DEBUG     0

#define ESP8285_BAUD_RATE       115200
#define ESP                     Serial2

int wifi_rst = 33;
int wifi_mode = 39;

// -----------------------------------------------------------------------------
// Initialize class states and variables
SerialATClass::SerialATClass(int rst, int mode)
{
    _rst_pin = rst;
    _mode_pin = mode;

    // Make sure the wifi module is in reset
    digitalWrite(_rst_pin, LOW);
    // The mode pin must be high else the ESP8285 will enter flash download mode
    digitalWrite(_mode_pin, HIGH);
    pinMode(_rst_pin, OUTPUT);
    pinMode(_mode_pin, OUTPUT);
    _initialized = false;
    _pcb = NULL;
}

SerialATClass::SerialATClass()
{
    _rst_pin = 33;
    _mode_pin = 39;

    // Make sure the wifi module is in reset
    digitalWrite(_rst_pin, LOW);
    // The mode pin must be high else the ESP8285 will enter flash download mode
    digitalWrite(_mode_pin, HIGH);
    pinMode(_rst_pin, OUTPUT);
    pinMode(_mode_pin, OUTPUT);
    _initialized = false;
    _pcb = NULL;
}

// -----------------------------------------------------------------------------
// Initialize the serial port.
// For now we jut assume that this code will run on a Challenger board
// and that the ESP8285 has the proper firmware installed.
// So no errors are ever thrown
void SerialATClass::init()
{
    // On the Challenger board we always use serial port 2 at 115200.
    ESP.begin(ESP8285_BAUD_RATE);
    ESP.setTimeout(5000);

    DBG_PRINT("Initializing AT Uart !");
    _initialized = true;

    // Release reset and wait for the ready prompt
    digitalWrite(_rst_pin, LOW);
    delay(2);
    digitalWrite(_rst_pin, HIGH);
    String rdy = ESP.readStringUntil('\n');
    while(!rdy.startsWith("ready")) {
        yield();
        Serial.println(rdy);
        rdy = ESP.readStringUntil('\n');
    }
}

// -----------------------------------------------------------------------------
// Stores the callback pointer for later use and then calls the main init 
// function.
void SerialATClass::init(parse_callback pcb)
{
    _pcb = pcb;
    init();
}


//
//
// Parse an at response from the wifi modem
// This will take care of the response and update necessary variables
void SerialATClass::parse_at_response(char *resp)
{
    // If the user has specified a call back, call it here
    if (_pcb)
      _pcb(resp);
}

//
// Sends an AT command and stores the resulting list in the at_buf, with
// at_ptrs that points to each incoming line. Is this good ? No idea.
// The function will call at parser to parse any response from the 8285.
int SerialATClass::send_at(const char *fmt, ...)
{
    int ret = false;
    char at_out[64];
    va_list myargs;
    va_start(myargs, fmt);

    if (!_initialized)
        init();

    /* Forward the '...' to vsprintf */
    vsprintf(at_out, fmt, myargs);
    ESP.printf("AT%s\r\n", at_out);
    INF_PRINT("S:AT%s", at_out);

    // Wait for "OK" or "ERROR" from the wifi chip
    ESP.readBytesUntil('\n', at_buf, 64);           // Read response
    char *trim = strchr(at_buf, '\r');              // Find first '\r'
    *trim = '\0';                                   // Terminate string there
    int rcvd = strlen(at_buf);                      // Count characters received
    int offset = 0;
    rcvd_lines = 0;
    at_ptrs[rcvd_lines] = &at_buf[offset];          // Store this line
    while (strncmp(&at_buf[offset], "OK", 2) && strncmp(&at_buf[offset], "ERROR", 5)) {
        INF_PRINT("R:%s", &at_buf[offset]);
        parse_at_response(&at_buf[offset]);
        offset += rcvd + 1; // +1 because of the line terminator (0)
        ESP.readBytesUntil('\n', &at_buf[offset], 64);
        trim = strchr(&at_buf[offset], '\r');       // Find first '\r'
        *trim = '\0';                               // terminate string
        rcvd = strlen(&at_buf[offset]);             // How many bytes were rcvd
        rcvd_lines++;
        at_ptrs[rcvd_lines] = &at_buf[offset];
    }
    if (!strncmp(&at_buf[offset], "OK", 2)) ret = true;
    INF_PRINT("C:%s\n", &at_buf[offset]);

    /* Clean up va_list */
    va_end(myargs);

    return ret;
}

//
// This function needs to be called repetadly from the main loop.
// It is responsible for handling incoming unsolicited messages from
// the ESP8285.
void SerialATClass::handle_at()
{
    char *trim;
    if (!_initialized)
        init();
    // If there's data on the port we need to get it and process it.
    // Here we only process one line at a time.
    if (ESP.available()) {
        ESP.readBytesUntil('\n', at_buf, 64);
        trim = strchr(at_buf, '\r');       // Find first '\r'
        *trim = '\0';                               // terminate string
        DBG_PRINT("Unsolicited data: %s\n", at_buf);
        parse_at_response(at_buf);
    }
}
