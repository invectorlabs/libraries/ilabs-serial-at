/*
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "SerialAT.h"

// Instantiate Serial AT controller class
SerialATClass AT;

// Call back function from the AT controller that allows local AT response parsing
void at_parser(char *)
{
  // Do some AT parsing
  Serial.println("Local AT response parsing done here !");
}

void setup() {
  // Initialize the at serial AT controller
  //AT.init(at_parser);   // This line hooks the local parser
  AT.init();              // Without local parser

  // Terminal serial channel
  Serial.begin(115200);
  while (!Serial);

  Serial.println("Type some AT commands here !");
}

static char buf[64];
void loop() {
  // To take care of unsolicited messages from the modem 
  // the handle_at function needs to be called repeatedly.
  AT.handle_at();

  // Take AT commands from the terminal and send it to the modem.
  if (Serial.available()) {
    size_t len = Serial.readBytesUntil('\n', buf, 64);
    if (AT.send_at("%s\n", buf)) {
      Serial.println("Command successfull !");
    } else {
      Serial.println("Command failed !");
    }
  }
}
